﻿using System;
using System.Collections.Generic;

// message list handler with methods for the message list
namespace Sampling_messages_asp.Models
{
    public sealed class MessageList
    {
        // list which contains all sampling messages
        private List<SamplingMessage> _smList = new List<SamplingMessage>();
        
        // singleton instance
        private static volatile MessageList _instance;
        
        // mutex for blocking thread
        private static object syncRoot = new object();
        
        // max count of saved messages
        private const int MAX_MESSAGES = 32;

        private MessageList() {}

        // multi threaded singleton implementation
        public static MessageList MessageListInstance
        {
            get 
            {
                if (_instance != null) return _instance;
                
                // locking creation of singleton instance using syncRoot mutex
                lock (syncRoot)
                {
                    if (_instance == null)
                        _instance = new MessageList();
                }

                return _instance;
            }
        }

        // add a sampling message to the list
        public bool AddMessage(string name, int duration)
        {
            if (!(_smList.Count <= MAX_MESSAGES)) return false;
            if (_smList.Find(x => x.Name.Equals(name)) != null) return false;
            
            var sm = new SamplingMessage
            {
                Name = name,
                Updated = DateTime.Now,
                Duration = duration,
                Valid = true
            };
            
            _smList.Add(sm);
            return true;

        }

        // returns the sampling message with the given name
        public SamplingMessage GetMessage(string name)
        {
            return _smList.Find(x => x.Name.Equals(name));
        }

        // sets the data value of the sampling message with the given name
        public void SetData(string name, string data)
        {
            var sm = _smList.Find(x => x.Name.Equals(name));
            sm.Data = data;
            sm.Updated = DateTime.Now;
            sm.Valid = true;
        }

        // deletes the data value of the sampling message with the given name and invalids the message
        public void DeleteData(string name)
        {
            var sm = _smList.Find(x => x.Name.Equals(name));
            sm.Data = null;
            sm.Valid = false;
        }

        // deletes the sampling message with the given name
        public void DeleteMessage(string name)
        {
            _smList.RemoveAll(x => x.Name.Equals(name));
        }
    }
}