﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

// log handler with methods for the log list
namespace Sampling_messages_asp.Models
{
    internal sealed class Log
    {
        // contains all entries of the log
        private List<LogEntry> _logList = new List<LogEntry>();
        
        // is the singleton instance of this class
        private static volatile Log _instance;
        
        // mutex for blocking thread
        private static object syncRoot = new object();

        private Log() {}

        // multi threaded singleton implementation
        public static Log LogInstance
        {
            get 
            {
                if (_instance != null) return _instance;
                
                // locking creation of singleton instance using syncRoot mutex
                lock (syncRoot)
                {
                    if (_instance == null)
                        _instance = new Log();
                }

                return _instance;
            }
        }

        // method for cloning the list and not just passing the reference
        internal List<LogEntry> Clone()
        {
            var list = new List<LogEntry>();
            list.AddRange(_logList);
            return list;
        }

        // add one log entry to the log list
        public void AddLogEntry(HttpContext httpContext)
        {
            if (!httpContext.Request.Path.Equals("/samplingmessages/logs"))
            {
                _logList.Add(new LogEntry
                {
                    Ip = httpContext.Connection.RemoteIpAddress.ToString(),
                    HttpMethod = httpContext.Request.Method,
                    Path = httpContext.Request.Path
                });   
            }
        }

        // structual object which is saved in the log list
        internal class LogEntry
        {
            public string Ip { get; set; }
            public string HttpMethod { get; set; }
            public string Path { get; set; }
        }
    }
}