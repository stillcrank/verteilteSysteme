﻿using System;

// model object for a sampling message
namespace Sampling_messages_asp.Models
{
    public class SamplingMessage
    {
        private string _name;
        private DateTime _updated;
        private int _duration;
        private string _data;
        private bool _valid;

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public DateTime Updated
        {
            get => _updated;
            set => _updated = value;
        }

        public string Data
        {
            get => _data;
            set => _data = value;
        }

        public bool Valid
        {
            get => _valid;
            set => _valid = value;
        }

        public int Duration
        {
            get => _duration;
            set => _duration = value;
        }
    }
}