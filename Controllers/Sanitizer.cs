﻿using System.Text.RegularExpressions;

// checks incoming data (message name, value and duration)
namespace Sampling_messages_asp.Controllers
{
    internal class Sanitizer
    {
        // max length of message name
        private const int MAX_LETTER_NAME = 32;
        
        // max length of message value
        private const int MAX_LETTER_DATA = 255;

        internal bool SanitizeName(string name)
        {
            // name can hold between 1 and MAX_LETTER_NAME alphanumeric or underscore characters
            var regex = new Regex("^\\w{1," + MAX_LETTER_NAME + "}$");
            return regex.IsMatch(name);
        }

        internal bool SanitizeDuration(string duration)
        {
            if (string.IsNullOrEmpty(duration))
                return false;
            
            // duration mustn't begin with a zero and must contain only digits
            var regex = new Regex("^[1-9]\\d*$");
            return regex.IsMatch(duration);
        }

        internal bool SanitizeData(string data)
        {
            if (string.IsNullOrEmpty(data))
                return false;
            
            // value can contain any character up to MAX_LETTER_DATA times
            var regex = new Regex("^.{1," + MAX_LETTER_DATA + "}$");
            return regex.IsMatch(data);
        }
    }
}