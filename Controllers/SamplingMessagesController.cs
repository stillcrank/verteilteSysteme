﻿using Microsoft.AspNetCore.Mvc;
using static Sampling_messages_asp.Models.MessageList;
using static Sampling_messages_asp.Models.Log;


// Controller which handles the incoming requests at valid routes
namespace Sampling_messages_asp.Controllers
{
    // defines the route as name of the controller class/file
    [Route("[controller]")]
    public class SamplingMessagesController : Controller
    {   
        // Sanatizes all input and limits name to 32 and data to 255 letters
        private readonly Sanitizer _sanitizer = new Sanitizer();
        
        // Read sampling message
        // GET samplingmessages/exampleName
        [HttpGet("{name}")]
        public IActionResult Read(string name)
        {
            if (_sanitizer.SanitizeName(name))
            {
                // MessageListInstance is singleton instance for handling the saved sampling messages (see using statements above)
                var sm = MessageListInstance.GetMessage(name);
                if (sm != null)
                {
                    if (!string.IsNullOrEmpty(sm.Data) && !string.IsNullOrWhiteSpace(sm.Data))
                    {
                        return Ok(new ItemRead { 
                            Data = sm.Data, 
                            Valid = sm.Valid, 
                            Until = sm.Updated.AddSeconds(sm.Duration).ToShortDateString() + " " + sm.Updated.AddSeconds(sm.Duration).ToLongTimeString() 
                        });    
                    }
                    else
                    {
                        return NoContent();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest("bad parameters");
            }
        }
        
        // Write sampling message data
        // POST samplingmessages/exampleName
        // expects JSON with one key "value" and the corresponding data
        [HttpPost("{name}")]
        public IActionResult Write(string name, [FromBody] WriteData data)
        {
            if (data != null && _sanitizer.SanitizeName(name) && _sanitizer.SanitizeData(data.Value))
            {
                if (MessageListInstance.GetMessage(name) != null)
                {
                    MessageListInstance.SetData(name, data.Value);
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest("bad parameters");
            }
        }
    
        // Create sampling message
        // PUT samplingmessages/exampleName?duration=3600
        // PUT Content-Type must be application/json
        [HttpPut("{name}")]
        public IActionResult Create(string name, [FromQuery] string duration)
        {
            if (_sanitizer.SanitizeName(name) && _sanitizer.SanitizeDuration(duration))
            {
                if (MessageListInstance.AddMessage(name, int.Parse(duration)))
                {
                    return Created("samplingmessages", name);
                }
                else
                {
                    return BadRequest("already exists");
                }    
            }
            else
            {
                return BadRequest("bad parameters");
            }
        }
        
        // Delete sampling message
        // DELETE samplingmessages/exampleName
        [HttpDelete("{name}")]
        public IActionResult Delete(string name)
        {
            if (_sanitizer.SanitizeName(name))
            {
                if (MessageListInstance.GetMessage(name) != null)
                {
                    MessageListInstance.DeleteMessage(name);
                    return Ok();    
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest("bad parameters");
            }
        }
        
        // Return status
        // GET samplingmessages/exampleName/status
        [HttpGet("{name}/status")]
        public IActionResult Status(string name)
        {
            if (_sanitizer.SanitizeName(name))
            {
                if (MessageListInstance.GetMessage(name) != null)
                {
                    var sm = MessageListInstance.GetMessage(name);
                    return Ok(new ItemStatus { 
                        HasValue = !string.IsNullOrEmpty(sm.Data) && !string.IsNullOrWhiteSpace(sm.Data),
                        Valid = sm.Valid,
                        Until = sm.Updated.AddSeconds(sm.Duration).ToShortDateString() + " " + sm.Updated.AddSeconds(sm.Duration).ToLongTimeString()
                    });
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest("bad parameters");
            }
        }
        
        // Clear message and make it invalid
        // DELETE samplingmessages/exampleName/data
        [HttpDelete("{name}/data")]
        public IActionResult Clear(string name)
        {
            if (_sanitizer.SanitizeName(name))
            {
                if (MessageListInstance.GetMessage(name) != null)
                {
                    MessageListInstance.DeleteData(name);
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest("bad parameters");
            }
        }

        // Returns logs for every request made
        // GET samplingmessages/logs
        [HttpGet("logs")]
        public IActionResult Logs()
        {
            return Ok(LogInstance.Clone());
        }
        
        // JSON which is returned when reading a sampling message
        private class ItemRead
        {
            public string Data { get; set; }
            public bool Valid { get; set; }
            public string Until { get; set; }
        }

        // JSON which is return when reading the status of a sampling message
        private class ItemStatus
        {
            public bool HasValue { get; set; }
            public bool Valid { get; set; }
            public string Until { get; set; }
        }

        // JSON which is expected when writing data to a message (see write method)
        public class WriteData
        {
            public string Value { get; set; }
        }
    }
}