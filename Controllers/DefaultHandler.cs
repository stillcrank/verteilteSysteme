﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using static Sampling_messages_asp.Models.Log;

// Controller which handles the incoming requests before the samplingmessagescontroller takes the request
namespace Sampling_messages_asp.Controllers
{
    // takes every request and adds data to the log list
    public class DefaultHandler
    {
        private readonly RequestDelegate _next;
        
        public DefaultHandler(RequestDelegate next)
        {
            _next = next;
        }
        
        public Task Invoke(HttpContext httpContext)
        {
            // loginstance is singleton instance for handling the log list
            LogInstance.AddLogEntry(httpContext);
            return _next(httpContext);
        }
    }
    
    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class MyMiddlewareExtensions
    {
        public static IApplicationBuilder UseDefaultHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<DefaultHandler>();
        }
    }
}